const { SlashCommandBuilder } = require('@discordjs/builders');
const sqlite = require('sqlite3').verbose();
let sql;

module.exports = {
    data: new SlashCommandBuilder()
        .setName('usetpoints')
        .setDescription('Sets the points for the provided user.')
        .addUserOption(option => option.setName('user').setDescription('The user whose points will be changed').setRequired(true))
        .addIntegerOption(option => option.setName('points').setDescription('The points to set for the user').setRequired(true)),
    async execute(interaction) {
        const db = new sqlite.Database('./roles.db', sqlite.OPEN_READWRITE,(err)=>{
            if (err) return console.error(err.message);
        });

        const user = interaction.options.getUser('user').id;
        const points = interaction.options.getInteger('points');

        sql = `UPDATE leaderboard SET points=? WHERE user=?`
        db.run(sql, [points, user], (err) => {
            if (err) return console.error(err.message);
            interaction.reply({ content: 'Points updated. Leaderboard will update on next task approval.', ephemeral: true });
        })
    },
};