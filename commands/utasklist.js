const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageEmbed } = require("discord.js");
const sqlite = require('sqlite3').verbose();
let sql;

module.exports = {
    data: new SlashCommandBuilder()
        .setName('utasklist')
        .setDescription('Lists the current configured task modules.'),
    async execute(interaction) {
        const db = new sqlite.Database('./roles.db', sqlite.OPEN_READWRITE,(err)=>{
            if (err) return console.error(err.message);
        });
        let tasks = '';

        let resolve;
        let promise = new Promise(res => resolve = res);
        sql = `SELECT * FROM tasks`;
        db.all(sql, [], (err, rows) => {
            if (err) return console.error(err.message);
            if (rows.length !== 0) {
                rows.forEach(row => {
                    tasks += `${row.id}: ${row.task}\n`;
                })
            } else {
                tasks = '[ No tasks have been configured. Use /addtask to begin. ]';
            }
            resolve();
        })
        await promise;

        const embed = new MessageEmbed()
            .setColor('#3c7a33')
            .setTitle('Task List')
            .setDescription(tasks)
            .setTimestamp()
            .setFooter({ text: 'Sigil Utility Bot | Developed by Sanguine' });

        interaction.reply({ embeds: [embed] });
    },
};