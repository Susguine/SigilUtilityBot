const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageEmbed } = require("discord.js");
const sqlite = require('sqlite3').verbose();
let sql;

module.exports = {
    data: new SlashCommandBuilder()
        .setName('ugeneratetask')
        .setDescription('Generates a random task.'),
    async execute(interaction) {
        const db = new sqlite.Database('./roles.db', sqlite.OPEN_READWRITE,(err)=>{
            if (err) return console.error(err.message);
        });

        let tasks = [];
        let currentTask;
        let output = '[ No tasks have been configured. Use /addtask to begin. ]';

        let resolve;
        let promise = new Promise(res => resolve = res);
        sql = `SELECT * FROM tasks`;
        db.all(sql, [], (err, rows) => {
            if (err) return console.error(err.message);
            if (rows.length !== 0) {
                output = '';

                rows.forEach(row => {
                    tasks.push(`${row.task}`);
                })

                currentTask = Math.floor(Math.random()*tasks.length)
                output += `**Task:**\n${tasks[currentTask]}\n`;
                tasks.splice(currentTask, 1);
            }
            resolve();
        })
        await promise;

        const embed = new MessageEmbed()
            .setColor('#3c7a33')
            .setTitle(`Generated Task:`)
            .setDescription(output)
            .setTimestamp()
            .setFooter({ text: 'Sigil Utility Bot | Developed by Sanguine' });

        interaction.reply({ embeds: [embed] });
    },
};