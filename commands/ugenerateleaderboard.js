const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageEmbed } = require("discord.js");
const sqlite = require('sqlite3').verbose();
let sql;

module.exports = {
    data: new SlashCommandBuilder()
        .setName('ugenerateleaderboard')
        .setDescription('Generates the leaderboard for the task system.'),
    async execute(interaction) {
        const db = new sqlite.Database('./roles.db', sqlite.OPEN_READWRITE,(err)=>{
            if (err) return console.error(err.message);
        });

        let message;

        sql = `DELETE FROM board`;
        db.run(sql, [], (err) => {
            if (err) return console.error(err.message);
        })

        const embed = new MessageEmbed()
            .setColor('#3c7a33')
            .setTitle(`Task Completion Leaderboard`)
            .setTimestamp()
            .setFooter({ text: 'Sigil Utility Bot | Developed by Sanguine' });

        let resolve;
        let promise = new Promise(res => resolve = res);
        interaction.channel.send({ embeds: [embed] }).then(m => {
            message = m.id
            resolve();
        });
        await promise;

        sql = `INSERT INTO board(message, channel) VALUES (?, ?)`
        db.run(sql,[message, interaction.channel.id],(err)=>{
            if (err) {
                interaction.reply({ content: 'generateleaderboard board database error. talk to sanguine.', ephemeral: true })
                return console.error(err.message);
            } else {
                interaction.reply({ content: 'Leaderboard created.', ephemeral: true });
            }
        });
    },
};