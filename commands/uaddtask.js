const { SlashCommandBuilder } = require('@discordjs/builders');
const sqlite = require('sqlite3').verbose();
let sql;

module.exports = {
    data: new SlashCommandBuilder()
        .setName('uaddtask')
        .setDescription('Adds tasks to the task list.')
        .addStringOption(option => option.setName('string').setDescription('Task to be added to the task list')),
    async execute(interaction) {
        const db = new sqlite.Database('./roles.db', sqlite.OPEN_READWRITE,(err)=>{
            if (err) return console.error(err.message);
        });

        const task = interaction.options.getString('string');

        sql = `INSERT INTO tasks(task) VALUES (?)`
        db.run(sql, [task], (err) => {
            if (err) return console.error(err.message);
            interaction.reply({ content: 'Task added to the task list.', ephemeral: true });
        })
    },
};