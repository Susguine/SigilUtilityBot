const { SlashCommandBuilder } = require('@discordjs/builders');
const sqlite = require('sqlite3').verbose();
const { MessageEmbed } = require('discord.js');
let sql;

module.exports = {
    data: new SlashCommandBuilder()
        .setName('urolelist')
        .setDescription('Views the roles on the reset list.'),
    async execute(interaction) {
        const db = new sqlite.Database('./roles.db', sqlite.OPEN_READWRITE,(err)=>{
            if (err) return console.error(err.message);
        });

        let rolelist = '';
        let colonial = '';

        let resolve;
        let promise = new Promise(res => resolve = res);
        sql = `SELECT * FROM roles`;
        db.all(sql, [], (err, rows) => {
            if (err) return console.error(err.message);
            rows.forEach(row=>{
                rolelist += (`<@&${row.role}>\n`);
            })
            resolve();
        })
        await promise;

        promise = new Promise(res => resolve = res);
        sql = `SELECT * FROM mainrole`;
        db.all(sql, [], (err, rows) => {
            if (err) return console.error(err.message);
            if (rows[0] !== undefined){
                colonial = `<@&${rows[0].role}>`;
            }
            resolve();
        })
        await promise;

        const embed = new MessageEmbed()
            .setColor('#3c7a33')
            .setTitle('Role Reset List')
            .setTimestamp()
            .setFooter({ text: 'Sigil Utility Bot | Developed by Sanguine' });

        setTimeout(() => {
            if (colonial === '') {
                colonial = '[ No value for Colonial has been set. Use /colonial. ]';
            }
            if (rolelist === '') {
                rolelist = '[ No roles for have been set. Use /addrole. ]';
            }

            embed.addFields(
                { name: 'Colonial Role:', value: colonial },
                { name: 'Role Removal List:', value: rolelist }
            );

            interaction.reply({ embeds: [embed] })
        }, 100);
    },
};