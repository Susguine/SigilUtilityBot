const { SlashCommandBuilder } = require('@discordjs/builders');
const sqlite = require('sqlite3').verbose();
let sql;

module.exports = {
    data: new SlashCommandBuilder()
        .setName('ucolonial')
        .setDescription('Sets the Colonial role for role removal.')
        .addRoleOption(option => option.setName('role').setDescription('Enter the Colonial role')),
    async execute(interaction) {
        const db = new sqlite.Database('./roles.db', sqlite.OPEN_READWRITE,(err)=>{
            if (err) return console.error(err.message);
        });

        const role = interaction.options.getRole('role').id;

        sql = `DELETE FROM mainrole`;
        db.run(sql, [], (err) => {
            if (err) return console.error(err.message);
        })

        sql = `INSERT INTO mainrole(role) VALUES (?)`
        db.run(sql,[role],(err)=>{
            if (err) {
                interaction.reply('Failed to set the Colonial role.')
                return console.error(err.message);
            }
            interaction.reply('Colonial role has been set.');
        });
    },
};