const { SlashCommandBuilder } = require('@discordjs/builders');
const sqlite = require('sqlite3').verbose();
let sql;

module.exports = {
    data: new SlashCommandBuilder()
        .setName('uaddrole')
        .setDescription('Adds a role to the reset list to be removed each war.')
        .addRoleOption(option => option.setName('role').setDescription('Role to be added to the reset list')),
    async execute(interaction) {
        const db = new sqlite.Database('./roles.db', sqlite.OPEN_READWRITE,(err)=>{
            if (err) return console.error(err.message);
        });

        const role = interaction.options.getRole('role').id;

        let values = [];

        let resolve;
        let promise = new Promise(res => resolve = res);
        sql = `SELECT * FROM roles`;
        db.all(sql, [], (err, rows) => {
            if (err) return console.error(err.message);
            rows.forEach(row=>{
                values.push(row.role);
            })
            resolve();
        })
        await promise;

        if (values.includes(`${role.id}`)) {
            interaction.reply('This role is already in the reset list.');
        } else {
            sql = `INSERT INTO roles(role) VALUES (?)`
            db.run(sql,[role],(err)=>{
                if (err) {
                    interaction.reply('Failed to add role to the reset list.')
                    return console.error(err.message);
                }
                interaction.reply('Role added to the reset list.');
            });
        }
    },
};