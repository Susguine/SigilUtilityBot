const { SlashCommandBuilder } = require('@discordjs/builders');
const sqlite = require('sqlite3').verbose();
let sql;

module.exports = {
    data: new SlashCommandBuilder()
        .setName('uremoverole')
        .setDescription('Removes a role from the reset list to be removed each war.')
        .addRoleOption(option => option.setName('role').setDescription('Role to be removed from the reset list')),
    async execute(interaction) {
        const db = new sqlite.Database('./roles.db', sqlite.OPEN_READWRITE,(err)=>{
            if (err) return console.error(err.message);
        });

        const role = interaction.options.getRole('role');

        sql = `DELETE FROM roles WHERE role=?`;
        db.run(sql, [role.id], (err) => {
            if (err) {
                interaction.reply('Failed to remove role from the reset list.')
                return console.error(err.message);
            }
            interaction.reply('Role removed from the reset list.')
        })
    },
};