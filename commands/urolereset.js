const { SlashCommandBuilder } = require('@discordjs/builders');
const sqlite = require('sqlite3').verbose();
let sql;

module.exports = {
    data: new SlashCommandBuilder()
        .setName('urolereset')
        .setDescription('Resets roles for the new war.'),
    async execute(interaction) {
        const db = new sqlite.Database('./roles.db', sqlite.OPEN_READWRITE,(err)=>{
            if (err) return console.error(err.message);
        });

        let colonial;
        let roles = [];

        let resolve;
        let promise = new Promise(res => resolve = res);
        sql = `SELECT * FROM roles`;
        db.all(sql, [], (err, rows) => {
            if (err) return console.error(err.message);
            rows.forEach(row=>{
                if (row !== undefined) {
                    roles.push(interaction.guild.roles.cache.find(r => r.id === row.role));
                }
            })
            resolve();
        })
        await promise;

        promise = new Promise(res => resolve = res);
        sql = `SELECT * FROM mainrole`;
        db.all(sql, [], (err, rows) => {
            if (err) return console.error(err.message);
            rows.forEach(() => {
                if (rows[0]) {
                    colonial = interaction.guild.roles.cache.find(r => r.id === rows[0].role);
                }
            })
            resolve();
        })
        await promise;

        promise = new Promise(res => resolve = res);
        sql = `SELECT * FROM mainrole`;
        db.all(sql, [], (err, rows) => {
            if (err) return console.error(err.message);
            if (rows[0]) {
                colonial = interaction.guild.roles.cache.find(r => r.id === rows[0].role);
            }
            resolve();
        })
        await promise;

        let removals = 0;
        const colonialList = colonial.members.map(member => member);

        interaction.reply({ content: `Working on resetting roles. Standby.`, fetchReply: true }).then(msg => {
            colonialList.forEach((member) => {
                roles.forEach((role) => {
                    setTimeout(() => {
                        member.roles.remove(role);
                    }, 100)
                });
                removals++;
                setTimeout(() => {
                    member.roles.remove(colonial);
                }, 100);
            });
            msg.edit(`${removals} Colonials nuked from the server!`);
        });
    },
};