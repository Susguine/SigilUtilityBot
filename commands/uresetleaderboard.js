const { SlashCommandBuilder } = require('@discordjs/builders');
const sqlite = require('sqlite3').verbose();
let sql;

module.exports = {
    data: new SlashCommandBuilder()
        .setName('uresetleaderboard')
        .setDescription('Resets the leaderboard.'),
    async execute(interaction) {
        const db = new sqlite.Database('./roles.db', sqlite.OPEN_READWRITE,(err)=>{
            if (err) return console.error(err.message);
        });

        sql = `DELETE FROM leaderboard`;
        db.run(sql, [], (err) => {
            if (err) return console.error(err.message);
            interaction.reply({ content: 'Leaderboard successfully reset.', ephemeral: true });
        })
    },
};