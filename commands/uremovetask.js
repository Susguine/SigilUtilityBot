const { SlashCommandBuilder } = require('@discordjs/builders');
const sqlite = require('sqlite3').verbose();
let sql;

module.exports = {
    data: new SlashCommandBuilder()
        .setName('uremovetask')
        .setDescription('Removes tasks from the task list.')
        .addIntegerOption(option => option.setName('id').setDescription('ID of the task to be removed')),
    async execute(interaction) {
        const db = new sqlite.Database('./roles.db', sqlite.OPEN_READWRITE,(err)=>{
            if (err) return console.error(err.message);
        });

        const id = interaction.options.getInteger('id');

        sql = `DELETE FROM tasks WHERE id=?`;
        db.run(sql, [id], (err) => {
            if (err) {
                interaction.reply({ content: 'Failed to remove task from the task list.', ephemeral: true });
                return console.error(err.message);
            }
            interaction.reply({ content: 'Task removed from the task list.', ephemeral: true });
        })
    },
};