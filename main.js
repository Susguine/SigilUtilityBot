const fs = require('node:fs');
const path = require('node:path');
const { Client, Collection, Intents, MessageEmbed } = require('discord.js');
require('dotenv').config();

const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MEMBERS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_MESSAGE_REACTIONS, Intents.FLAGS.GUILD_MEMBERS] });

const adminId = process.env.ADMIN_ID;
const guildId = process.env.GUILD_ID;
let leaderboard = [];
let guild;
client.guilds.fetch(guildId).then(g => { guild = g });

const sqlite = require('sqlite3').verbose();

if (!fs.existsSync('./roles.db')) {
    fs.writeFile('./roles.db', '', err => {
        if (err) return console.error(err.message);
    })
}

const db = new sqlite.Database('./roles.db', sqlite.OPEN_READWRITE,(err)=>{
    if (err) return console.error(err.message);
});

let adminRole;

client.commands = new Collection();
const commandsPath = path.join(__dirname, 'commands');
const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
    const filePath = path.join(commandsPath, file);
    const command = require(filePath);
    client.commands.set(command.data.name, command);
}

client.once('ready', () => {
    db.run(`CREATE TABLE IF NOT EXISTS roles(role)`);
    db.run(`CREATE TABLE IF NOT EXISTS mainrole(role)`);
    db.run(`CREATE TABLE IF NOT EXISTS adminrole(role)`);
    db.run(`CREATE TABLE IF NOT EXISTS tasks(id INTEGER PRIMARY KEY, task)`);
    db.run(`CREATE TABLE IF NOT EXISTS leaderboard(user, points)`);
    db.run(`CREATE TABLE IF NOT EXISTS board(message, channel)`);
    db.run(`CREATE UNIQUE INDEX IF NOT EXISTS leaderboard_user ON leaderboard(user)`, [], (err) => { if (err) return console.error(err.message) });

    let sql = `SELECT * FROM leaderboard`;
    db.all(sql, [], (err, rows) => {
        if (err) return console.error(err.message);
        rows.forEach(row => {
            leaderboard.push({
                user: row.user,
                points: row.points
            });
        })

        leaderboard.forEach((row, i) => {
            for (let j = 0; j < leaderboard.length-(i+1); j++) {
                if (parseInt(leaderboard[j].points) <= parseInt(leaderboard[j+1].points)) {
                    [leaderboard[j], leaderboard[j+1]] = [leaderboard[j+1], leaderboard[j]];
                }
            }
        })
    })

    setTimeout(() => {
        client.guilds.fetch(guildId).then(guild => {
            let sql;
            adminRole = guild.roles.cache.find(r => r.id === adminId);

            sql = `DELETE FROM adminrole`;
            db.run(sql, [], (err) => {
                if (err) return console.error(err.message);
            })

            sql = `INSERT INTO adminrole(role) VALUES (?)`
            db.run(sql,[adminRole.id],(err)=>{
                if (err) return console.error(err.message);
            });
        });
    }, 300);

    console.log('Ready!');
});

client.on('messageReactionAdd', async (reaction) => {
    if (reaction.count > 1) return;
    reaction.users.fetch().then(users => {
        users.forEach(user => {
            guild.members.fetch(user.id).then(async member => {
                if (member.roles.cache.has(adminRole.id) && reaction.message.channel.name.endsWith('submissions') && (reaction.emoji.name === '✅' || reaction.emoji.name === '☑️')) {
                    let resolve;
                    let lb = [];
                    let promise = new Promise(res => resolve = res);
                    let sql = `SELECT * FROM leaderboard`;
                    db.all(sql, [], (err, rows) => {
                        if (err) return console.error(err.message);
                        let loc = false;
                        let desync = false;
                        rows.forEach(row => {
                            if (row.user === reaction.message.author.id) {
                                loc = true;
                            }

                            if (leaderboard.indexOf(row) === -1) {
                                desync = true;
                            }
                        })
                        if (!loc) {
                            let sql2 = `INSERT INTO leaderboard(user, points) VALUES (?, ?)`;
                            db.run(sql2, [reaction.message.author.id, 0], (err) => {
                                if (err) return console.error(err.message);
                            })
                        }
                        if (desync) {
                            rows.forEach(row => {
                                lb.push(row);
                            })
                            lb.forEach((row, i) => {
                                for (let j = 0; j < lb.length-(i+1); j++) {
                                    if (parseInt(lb[j].points) <= parseInt(lb[j+1].points)) {
                                        [lb[j], lb[j+1]] = [lb[j+1], lb[j]];
                                    }
                                }
                            })

                            leaderboard = lb;
                        }
                        resolve();
                    })
                    await promise;

                    let points;

                    promise = new Promise(res => resolve = res);
                    sql = `SELECT points FROM leaderboard WHERE user =?`;
                    db.all(sql, [reaction.message.author.id], (err, rows) => {
                        if (err) return console.error(err.message);
                        points = parseInt(rows[0].points);
                        resolve();
                    })
                    await promise;

                    if (reaction.emoji.name === '✅') {
                        points += 1;
                    } else {
                        points += 5;
                    }

                    promise = new Promise(res => resolve = res);
                    sql = `UPDATE leaderboard SET points=? WHERE user=?`;
                    db.run(sql, [points, reaction.message.author.id], (err) => {
                        if (err) return console.error(err.message);
                        resolve();
                    })
                    await promise;


                    let loc = false;
                    for (let i = 0; i < leaderboard.length; i++) {
                        if (leaderboard[i].user === reaction.message.author.id) {
                            leaderboard[i].points = points;
                            loc = true;
                        }
                    }

                    if (loc === false) {
                        if (leaderboard.length === 0) {
                            leaderboard.push({ user: reaction.message.author.id, points: points });
                        } else {
                            leaderboard.forEach((row, i) => {
                                if (parseInt(row.points) <= parseInt(leaderboard[i].points)) {
                                    leaderboard.splice(i, 0, { user: reaction.message.author.id, points: points });
                                }
                            })
                        }
                    }

                    let output = '';

                    leaderboard.forEach(row => {
                        output += `<@${row.user}>: ${row.points}\n`;
                    })

                    const embed = new MessageEmbed()
                        .setColor('#3c7a33')
                        .setTitle(`Task Completion Leaderboard`)
                        .setDescription(output)
                        .setTimestamp()
                        .setFooter({ text: 'Sigil Utility Bot | Developed by Sanguine' });

                    let c;
                    let m;

                    sql = `SELECT * FROM board`;
                    db.all(sql, [], (err, rows) => {
                        if (err) return console.error(err.message);
                        if (rows.length !== 0) {
                            c = rows[0].channel;
                            m = rows[0].message;
                            client.channels.fetch(c).then(channel => {
                                channel.messages.fetch(m).then(message => {
                                    try {
                                        message.edit({embeds: [embed]});
                                    } catch {
                                        console.error(err);
                                    }
                                });
                            })
                        }
                    })
                }
            })
        })
    });
});

client.on('interactionCreate', async interaction => {
    if (!interaction.isCommand()) return;

    const command = client.commands.get(interaction.commandName);

    if (!command) return;

    if ((!interaction.member.roles.cache.has(adminRole.id)) && (interaction.commandName !== 'generatetask')) {
        interaction.reply({ content: 'You are not authorized to use this!', ephemeral: true });
        return;
    }

    try {
        await command.execute(interaction);
    } catch (err) {
        console.error(err);
        await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
    }
});

client.login(process.env.CLIENT_TOKEN);
